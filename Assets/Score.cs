﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    RaycastHit2D hit;
    public Text scoreText;
    //public GameObject box;
    private int score = 0;
    public GameObject SampahMasyarakat;
    BoxCollider2D sampah;
    public LayerMask tempatSampah;

    // Start is called before the first frame update
    void Start()
    {
        //scoreText = GetComponent<Text>();
        //box = GetComponent <GameObject>;
        sampah = transform.GetComponent<BoxCollider2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (TambahScore())
        {
            score += 1;
            scoreText.text = "Score : " + score.ToString();
            Destroy(SampahMasyarakat);
        }  
    }

    private bool TambahScore()
    {
        RaycastHit2D rc = Physics2D.BoxCast(sampah.bounds.center, sampah.bounds.size, 0f, Vector2.down, .1f, tempatSampah);
        Debug.Log("nambah");
        return rc.collider != null;
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "objek")
        {
            score++;
            scoreText.text = "Score : " + score.ToString();
        }
    }*/
}
